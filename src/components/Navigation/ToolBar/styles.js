import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  AppBar: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: "70px",
  },
  Admin: {
    cursor: "pointer",
  },
}));
