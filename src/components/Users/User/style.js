import { makeStyles } from "@material-ui/core/styles";

export default makeStyles(() => ({
  table: {
    minWidth: "650px",
  },
  icon: {
    cursor: "pointer",
  },
  Input: {
    margin: "10px",
  },
  Card: {
    padding: "10px",
    minWidth: "650px",
  },
}));
