import { makeStyles } from "@material-ui/core/styles";

export default makeStyles(() => ({
  table: {
    minWidth: "650px",
  },
  icon: {
    cursor: "pointer",
  },
  Card: {
    padding: "10px",
    minWidth: "650px",
  },
}));
